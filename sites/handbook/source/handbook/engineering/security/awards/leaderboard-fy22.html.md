---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY22

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@cablett](https://gitlab.com/cablett) | 1 | 500 |
| [@theoretick](https://gitlab.com/theoretick) | 2 | 400 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 3 | 300 |
| [@sabrams](https://gitlab.com/sabrams) | 4 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 5 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 6 | 200 |
| [@leipert](https://gitlab.com/leipert) | 7 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 8 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 9 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 10 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 11 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 12 | 140 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 13 | 110 |
| [@allison.browne](https://gitlab.com/allison.browne) | 14 | 100 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 15 | 100 |
| [@stanhu](https://gitlab.com/stanhu) | 16 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 17 | 100 |
| [@engwan](https://gitlab.com/engwan) | 18 | 80 |
| [@splattael](https://gitlab.com/splattael) | 19 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 20 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 21 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 22 | 80 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 23 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 24 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 25 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 26 | 80 |
| [@twk3](https://gitlab.com/twk3) | 27 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 28 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 29 | 80 |
| [@ahegyi](https://gitlab.com/ahegyi) | 30 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 31 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 32 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 33 | 60 |
| [@balasankarc](https://gitlab.com/balasankarc) | 34 | 60 |
| [@manojmj](https://gitlab.com/manojmj) | 35 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 36 | 60 |
| [@mwoolf](https://gitlab.com/mwoolf) | 37 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 38 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 39 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 40 | 40 |
| [@tomquirk](https://gitlab.com/tomquirk) | 41 | 40 |
| [@proglottis](https://gitlab.com/proglottis) | 42 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 43 | 30 |
| [@cngo](https://gitlab.com/cngo) | 44 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 45 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@smcgivern](https://gitlab.com/smcgivern) | 5 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 6 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |

## FY22-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@cablett](https://gitlab.com/cablett) | 1 | 500 |
| [@theoretick](https://gitlab.com/theoretick) | 2 | 400 |
| [@tmaczukin](https://gitlab.com/tmaczukin) | 3 | 300 |
| [@sabrams](https://gitlab.com/sabrams) | 4 | 300 |
| [@arturoherrero](https://gitlab.com/arturoherrero) | 5 | 280 |
| [@WarheadsSE](https://gitlab.com/WarheadsSE) | 6 | 200 |
| [@leipert](https://gitlab.com/leipert) | 7 | 200 |
| [@serenafang](https://gitlab.com/serenafang) | 8 | 200 |
| [@patrickbajao](https://gitlab.com/patrickbajao) | 9 | 200 |
| [@jerasmus](https://gitlab.com/jerasmus) | 10 | 140 |
| [@mksionek](https://gitlab.com/mksionek) | 11 | 140 |
| [@igor.drozdov](https://gitlab.com/igor.drozdov) | 12 | 140 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 13 | 110 |
| [@allison.browne](https://gitlab.com/allison.browne) | 14 | 100 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 15 | 100 |
| [@stanhu](https://gitlab.com/stanhu) | 16 | 100 |
| [@kerrizor](https://gitlab.com/kerrizor) | 17 | 100 |
| [@engwan](https://gitlab.com/engwan) | 18 | 80 |
| [@splattael](https://gitlab.com/splattael) | 19 | 80 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 20 | 80 |
| [@vsizov](https://gitlab.com/vsizov) | 21 | 80 |
| [@lauraMon](https://gitlab.com/lauraMon) | 22 | 80 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 23 | 80 |
| [@iamphill](https://gitlab.com/iamphill) | 24 | 80 |
| [@mparuszewski](https://gitlab.com/mparuszewski) | 25 | 80 |
| [@dustinmm80](https://gitlab.com/dustinmm80) | 26 | 80 |
| [@twk3](https://gitlab.com/twk3) | 27 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 28 | 80 |
| [@mcelicalderonG](https://gitlab.com/mcelicalderonG) | 29 | 80 |
| [@ahegyi](https://gitlab.com/ahegyi) | 30 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 31 | 60 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 32 | 60 |
| [@seanarnold](https://gitlab.com/seanarnold) | 33 | 60 |
| [@balasankarc](https://gitlab.com/balasankarc) | 34 | 60 |
| [@manojmj](https://gitlab.com/manojmj) | 35 | 60 |
| [@vyaklushin](https://gitlab.com/vyaklushin) | 36 | 60 |
| [@mwoolf](https://gitlab.com/mwoolf) | 37 | 40 |
| [@mrincon](https://gitlab.com/mrincon) | 38 | 40 |
| [@ck3g](https://gitlab.com/ck3g) | 39 | 40 |
| [@pslaughter](https://gitlab.com/pslaughter) | 40 | 40 |
| [@tomquirk](https://gitlab.com/tomquirk) | 41 | 40 |
| [@proglottis](https://gitlab.com/proglottis) | 42 | 30 |
| [@blabuschagne](https://gitlab.com/blabuschagne) | 43 | 30 |
| [@cngo](https://gitlab.com/cngo) | 44 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 45 | 30 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@nolith](https://gitlab.com/nolith) | 1 | 300 |
| [@reprazent](https://gitlab.com/reprazent) | 2 | 290 |
| [@kwiebers](https://gitlab.com/kwiebers) | 3 | 200 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 160 |
| [@smcgivern](https://gitlab.com/smcgivern) | 5 | 40 |
| [@aqualls](https://gitlab.com/aqualls) | 6 | 40 |

### Non-Engineering

Category is empty

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@emanuele.divizio](https://gitlab.com/emanuele.divizio) | 1 | 300 |


